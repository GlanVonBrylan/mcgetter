"use strict";

const { createServer } = require("http");
const Query = require("mcquery");

const PORT = 5858;
const WRONG_URI = "The request URI should be an address and port (ex: 128.15.54.12:25565).";

function returnResponse(res, statusCode, data) {
	res.statusCode = statusCode;
	res.end(data);
}

const server = createServer((req, res) => {
	if(req.method !== "GET")
	{
		res.setHeader("Allow", "GET");
		return returnResponse(res, req.method === "OPTIONS" ? 200 : 405);
	}

	if(decodeURIComponent(req.url).endsWith("Trinity.txt.bak"))
		return returnResponse(res, 400, "Piss off, nmap");

	if(req.url === "/favicon.ico")
		return returnResponse(res, 400, "Piss off, browser");

	if(req.url === "/" || req.url.lastIndexOf("/") !== 0)
		return returnResponse(res, 403, WRONG_URI);


	const addr = req.url.substring(1).split(":");
	const [host, port = 25565] = addr;

	if(!host || addr.length > 2 || (port !== undefined && !Number.isInteger(+port)))
		return returnResponse(res, 400, WRONG_URI);

	const query = new Query(host, +port);

	query.connect().then(() => {
		query.full_stat((err, stats) => {
			if(err)
			{
				console.error("error", err);
				returnResponse(res, 500, err.message);
			}
			else
			{
				if(stats.plugins)
				{
					const plugins = stats.plugins.split("; ");
					if(plugins[0])
						plugins[0] = plugins[0].split(": ")[1];
					stats.plugins = plugins;
				}
				res.setHeader("Content-Type", "application/json");
				returnResponse(res, 200, JSON.stringify(stats));
			}
			query.close();
		});
	}, err => {
		const {message} = err;
		if(!message.includes("Request timeout") && !message.includes("ENOTFOUND"))
			console.error("error connecting", err);
		returnResponse(res, 500, message);
	});
});


function launch()
{
	const {exec} = require("child_process");

	// In case a previous listener was left dangling...
	exec(`lsof -i TCP:${PORT} | grep LISTEN`, (err, stdout, stderr) => {
		if(stdout)
			exec("kill -9 " + stdout.match(/[0-9]+/), launch);
		else
		{
			server.listen(PORT);
			console.log(`Listening on port ${PORT}`);
		}
	});
}

launch();
